﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using WeatherForecast.Controller;

namespace WeatherForecast.Model
{
    // Forecast center based on using "OpenWeather" API
    class ForecastCenter
    {
        private const string appID = "27c894cd4df10e46b44afe567c2b15f9";
        private WebConnection webConnection;
        private JsonMapper jsonMapper;

        public ForecastCenter()
        {
            this.webConnection = new WebConnection();
            this.jsonMapper = new JsonMapper();
        }

        public WeatherSample GetWeatherSample(string city)
        {
            string url = "http://" + $"api.openweathermap.org/data/2.5/weather?q={city}&APPID={appID}&units=metric";
            string jsonResponse = webConnection.Get(url);

            return jsonMapper.JsonToWeatherSample(jsonResponse);
        }

        public Forecast GetForecast(string city)
        {
            string url = "http://" + $"api.openweathermap.org/data/2.5/forecast?q={city}&APPID={appID}&units=metric";
            string jsonResponse = webConnection.Get(url);

            return jsonMapper.JsonToForecast(jsonResponse);
        }
     
    }
}
