﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WeatherForecast.Model;

namespace WeatherForecast.Controller
{
    class JsonMapper
    {
        public WeatherSample JsonToWeatherSample(string json)
        {
            return JsonConvert.DeserializeObject<WeatherSample>(json);
        }

        public Forecast JsonToForecast(string json)
        {
            return JsonConvert.DeserializeObject<Forecast>(json);
        }
    }
}
