﻿using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using WeatherForecast.Model;

namespace WeatherForecast
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public ObservableCollection<Forecast> CitiesReport { get; set; }
        public List<string> XLabels { get; set; }
        public WeatherSample CurrentCity { get; set; }

        private ForecastCenter forecastCenter;

        public MainWindow()
        {
            InitializeComponent();
            this.DataContext = this;
            this.CitiesReport = new ObservableCollection<Forecast>();
            this.forecastCenter = new ForecastCenter();
            this.XLabels = new List<string>();

            InitTable();
            InitCurrent();
        }

        private void InitTable()
        {
            Forecast f = this.forecastCenter.GetForecast("Novi Sad");
            this.CitiesReport.Add(f);
        }

        private void InitCurrent()
        {
            this.CurrentCity = this.forecastCenter.GetWeatherSample("Novi Sad");
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.UpdateGraph();
        }

        private void CloseApplication(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }

        private void AddCity(object sender, RoutedEventArgs e)
        {
            Forecast newForecast;
            try
            { 
                newForecast = this.forecastCenter.GetForecast(SearchCity.Text);
                this.AddForecast(newForecast);        
            }
            catch
            {
                MessageBox.Show("Selected City does not exist!");
            }
        }

        private void AddForecast(Forecast newForecast)
        {
            if(newForecast.Cod == 200)
            {
                this.TrimForecast(newForecast);

                if (!this.CitiesReport.Contains(newForecast))
                {
                    this.CitiesReport.Add(newForecast);
                    if (this.CitiesReport.Count == 1)
                        this.UpdateCurrentCity(this.CitiesReport[0].City.Name);
                    this.UpdateGraph();
                }
                else
                    MessageBox.Show($"{newForecast.City.Name} is already in Table!");
            }
            else
            {
                MessageBox.Show("Forecast error!");
            }
        }

        private void TrimForecast(Forecast newForecast)
        {
            int days = this.GetPeriod();

            DateTime now = DateTime.Now;
            DateTime border = now.AddDays(days);

            for (int i = newForecast.List.Count - 1; i >= 0; i--)
                if (newForecast.List[i].Date > border)
                    newForecast.List.RemoveAt(i);
        }

        private int GetPeriod()
        {
            string text = Period.Text;
            int period = 1;

            switch(text)
            {
                case "One day": period = 1;break;
                case "Two days": period = 2;break;
                case "Three days": period = 3;break;
                case "Four days": period = 4;break;
                case "Five days": period = 5;break;
            }

            return period;
        }

        private void UpdateGraph()
        {
            Graph.Series.Clear();
            SeriesCollection series = new SeriesCollection();

            foreach(Forecast f in this.CitiesReport)
            {
                List<double> values = GetGraphInfo(f);
                this.SetXAxis(f);
                series.Add(new LineSeries
                {
                    Title = f.City.Name,
                    Values = new ChartValues<double>(values)
                });
            }

            Graph.Series = series;
        }

        private void SetXAxis(Forecast f)
        {
            foreach (WeatherSample ws in f.List)
                XLabels.Add(ws.Date.ToString());
        }

        private List<double> GetGraphInfo(Forecast f)
        {
            string param = GraphParam.Text;
            List<double> values = new List<double>();

            switch(param)
            {
                case "Temperature": values = (from v in f.List select v.Main.Temp).ToList(); break;
                case "Presure": values = (from v in f.List select v.Main.Pressure).ToList(); break;
                case "Humidity": values = (from v in f.List select v.Main.Humidity).ToList(); break;
            }

            return values;
        }

        private void SidebarExpanded(object sender, RoutedEventArgs e)
        {
            GraphBox.SetValue(Grid.ColumnProperty, 1);
        }

        private void SidebarCollapsed(object sender, RoutedEventArgs e)
        {
            GraphBox.SetValue(Grid.ColumnProperty, 0);
        }

        public void AddText(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(SearchCity.Text))
                SearchCity.Text = "Enter City name...";
        }

        private void RemoveText(object sender, RoutedEventArgs e)
        {
            if (SearchCity.Text == "Enter City name...")
            {
                SearchCity.Text = "";
            }
        }

        public void RemoveCity(string cityName)
        {
            Forecast f = new Forecast();
            f.City.Name = cityName;
            this.CitiesReport.Remove(f);
            this.UpdateGraph();

            if (this.CurrentCity.Name == cityName)
                if (this.CitiesReport.Count != 0)
                {
                    string name = this.CitiesReport[CitiesReport.Count - 1].City.Name;
                    this.UpdateCurrentCity(name);
                }
                else
                    this.EmptyCurrent();
        }

        private void ChangeGraphParam(object sender, SelectionChangedEventArgs e)
        {
            if(Graph != null)
            {
                string text = (e.AddedItems[0] as ComboBoxItem).Content as string;
                GraphParam.Text = text;
                this.UpdateGraph();
            }
        }

        private void ChangeCurrent(object sender, SelectionChangedEventArgs e)
        {
            string text = NewCity.SelectedValue as string;

            if (text != null && text != "")
                this.UpdateCurrentCity(text);

        }

        private void UpdateCurrentCity(string text)
        {
            this.CurrentCity = this.forecastCenter.GetWeatherSample(text);

            CCityName.Text = this.CurrentCity.Name;
            CCityTemp.Text = this.CurrentCity.Main.Temp.ToString();
            CCityFill.Text = this.CurrentCity.Main.Feels_like.ToString();
            CCityVis.Text = this.CurrentCity.Visibility.ToString();
            CCityWind.Text = this.CurrentCity.Wind.Speed.ToString();

            NewCity.Text = text;
        }

        private void EmptyCurrent()
        {
            CCityName.Text = "City name";
            CCityTemp.Text = "0";
            CCityFill.Text = "0";
            CCityVis.Text = "0";
            CCityWind.Text = "0";
        }
    }
}
