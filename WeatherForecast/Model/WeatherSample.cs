﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherForecast.Model
{
    public class WeatherSample
    {
        public string Name { get; set; }
        public int Dt { get; set; }
        public int Visibility { get; set; }
        public List<Weather> Weather;
        public Main Main { get; set; }
        public Wind Wind { get; set; }
        public Clouds Clouds { get; set; }

        [JsonProperty("dt_txt")]
        public DateTime Date { get; set; }
    }

    public class Weather
    {
        public int id;
        public string main;
        public string description;
        public string icon;
    }

    public class Main
    {
        public double Temp { get; set; }
        public double Feels_like { get; set; }
        public double Temp_min { get; set; }
        public double Temp_max { get; set; }
        public double Pressure { get; set; }
        public double Humidity { get; set; }
    }

    public class Wind
    {
        public double Speed { get; set; }
        public double deg;
    }

    public class Clouds
    {
        public double all;
    }

}
