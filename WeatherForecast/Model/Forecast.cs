﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WeatherForecast.Model
{
    public class Forecast
    {
        public int Cod { get; set; }
        public int Message { get; set; }
        public int Cnt { get; set; }
        public City City { get; set; }
        public ObservableCollection<WeatherSample> List { get; set; }

        public override bool Equals(Object obj)
        {
            //Check for null and compare run-time types.
            if ((obj == null) || !this.GetType().Equals(obj.GetType()))
            {
                return false;
            }
            else
            {
                Forecast f  = (Forecast) obj;
                return this.City.Name == f.City.Name;
            }
        }

        public Forecast()
        {
            this.City = new City();
        }
    }

    public class City
    {
        public string Name { get; set; }
    }
}
